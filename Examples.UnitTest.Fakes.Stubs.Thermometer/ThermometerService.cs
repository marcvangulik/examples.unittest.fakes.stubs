﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Examples.UnitTest.Fakes.Stubs.Contracts;

namespace Examples.UnitTest.Fakes.Stubs.Thermometer
{
    /// <summary>
    /// Class for temperature reading functionality
    /// </summary>
    public class ThermometerService : ITemperatureService 
    {
        /// <summary>
        /// Gets the current temperature.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <returns></returns>
        public float GetCurrentTemperature(string location)
        {
            //just return a fixed temperature
            return 20;
        }
    }
}
