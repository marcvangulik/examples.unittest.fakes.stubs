﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Examples.UnitTest.Fakes.Stubs.Contracts;

namespace Examples.UnitTest.Fakes.Stubs.ClothingAdvice
{
    /// <summary>
    /// Class implements advice rules about what trousers to wear.
    /// </summary>
    public class Trousers : ITrousersAdviceService 
    {
        ITemperatureService _temperatureService;

        /// <summary>
        /// Initializes a new instance of the <see cref="Trousers" /> class.
        /// </summary>
        /// <param name="temperatureService">The temperature service.</param>
        public Trousers(ITemperatureService temperatureService)
        {
            _temperatureService = temperatureService;
        }

        /// <summary>
        /// Gets the advice.
        /// </summary>
        /// <returns>The advice</returns>
        public TrousersType GetAdvice()
        {
            
            if (_temperatureService.GetCurrentTemperature(string.Empty) > 24)
            {
                return TrousersType.Short;
            }
            else
            {
                return TrousersType.Long;
            }
        }
    }
}
