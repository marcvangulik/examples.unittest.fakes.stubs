﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Examples.UnitTest.Fakes.Stubs.Contracts
{
    public enum TrousersType
    {
        Long,
        Short
    }
    /// <summary>
    /// Service definition for trousers advice functionality
    /// </summary>
    public interface ITrousersAdviceService
    {
        /// <summary>
        /// Gets the advice.
        /// </summary>
        /// <returns></returns>
        TrousersType GetAdvice();
    }
    /// <summary>
    /// Services definition for temperature functionality
    /// </summary>
    public interface ITemperatureService
    {
        float GetCurrentTemperature(string location);
    }
}
