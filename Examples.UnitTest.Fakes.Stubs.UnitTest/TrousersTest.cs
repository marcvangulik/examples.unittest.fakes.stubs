﻿using System;
using Examples.UnitTest.Fakes.Stubs.ClothingAdvice;
using Examples.UnitTest.Fakes.Stubs.Contracts;
using Examples.UnitTest.Fakes.Stubs.Contracts.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Examples.UnitTest.Fakes.Stubs.UnitTest
{
    /// <summary>
    /// Class for testing trousers functionality
    /// </summary>
    [TestClass]
    public class TrousersTest
    {
        /// <summary>
        /// Tests the trousers advice.
        /// </summary>
        [TestMethod]
        public void TestTrousersAdvice()
        {
            ITemperatureService temperatureService;
            Trousers trousers;
            
            // Cold weather flow
            temperatureService =
                new StubITemperatureService() // Generated by Fakes.
                { 
                    //Implement method stub
                    GetCurrentTemperatureString = (locaton) => { return 18; } //return 18 (cold) for temperature
                };

            trousers  = new Trousers(temperatureService);
            //Test 
            Assert.AreEqual(trousers.GetAdvice(), TrousersType.Long);

            // Warm weather flow
            temperatureService =
                new StubITemperatureService() // Generated by Fakes.
                {
                    //Implement method stub
                    GetCurrentTemperatureString = (locaton) => { return 25; } //return 24 (warm) for temperature
                };
            
            trousers = new Trousers(temperatureService);

            //Test 
            Assert.AreEqual(trousers.GetAdvice(), TrousersType.Short);

        }
    }
}
